package com.demo.gamesimulator.analyzer.service;


import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableKafkaStreams
@EnableKafka
public class AnalyzerStream {
    @Value("${bootstrap.servers}") private String bootStrap;
    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    public StreamsConfig kStreamsConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "analyzer-stream");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.Integer().getClass().getName());
        props.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class.getName());
        props.put("bootstrap.servers", bootStrap);
        return new StreamsConfig(props);
    }

    @Bean
    public KStream<String, Long> kStream(StreamsBuilder kStreamBuilder) {
        KStream<String, Integer> left = kStreamBuilder.stream("generated-value");
        KStream<String, Integer> right = kStreamBuilder.stream("player1-guess");
        KStream<String, Long> joined = left.leftJoin(right,
                (leftValue, rightValue) -> {return (leftValue == rightValue) ? "player-1-win" : "player-1-lose";},
                JoinWindows.of(TimeUnit.MINUTES.toMillis(5)),
                Serdes.String(),
                Serdes.Integer(),
                Serdes.Integer()
        ).filter((k,v) -> "player-1-win".equals(v))
                .peek((k ,v) -> System.out.println(k + "-" + v))
                .groupByKey()
                .count().toStream().map((k,v) -> new KeyValue<>("player1", v))
                .through("analyzed-result");
        return joined;
    }

}