# Analyzer

This app reads from `generated-value` and `player topics` topic and joins the two streams and filters
winning bids and passes them on to `analyzed-result` topic
### Prerequisites

What things you need to install the software and how to install them

```
1. You would need an instance of kafka(1.1.0 and higher) running.
   Update bootstrap.servers in application.yml to point towards your kafka instance.
2. You would need java8 installed
```

### Installing


```
1. ./gradlew bootRun would start your app.

2. You can also run the app through java -jar /build/libs/analyzer-0.0.1-SNAPSHOT.jar
```

The app runs indefinitely.
 
## Built With

* [Spring Docs](https://docs.spring.io/spring-kafka/docs/2.1.5.RELEASE/reference/html/)
* [Kafka Docs](https://docs.confluent.io/current/streams/index.html)
